import numpy as np
import torch
import matplotlib.pyplot as plt
from torch import nn, optim
import string
from torch.utils.data import(Dataset, DataLoader, TensorDataset)
import tqdm
from statistics import mean




#文字列を数値のリストに変換する関数             
def str2ints(s, vocab_dict):
    return [vocab_dict[c] for c in s]

#数値のリストを文字列に変換する関数    
def ints2str(x, allwords):
    return "".join([allwords[i] for i in x])
    
class Dataset(Dataset):
    def __init__(self, path, chunk_size):
        
        #ファイルから読み込んだ文章を数値のリストに変換する
        data = str2ints(words, vocab_dict)
       
        #Tensorに変換し、splitする
        data = torch.tensor(data, dtype=torch.int64).split(chunk_size)
        
        #最後のchunkの長さをチェックして足りない場合には捨てる
        if len(data[-1]) < chunk_size:
            data = data[:-1]
            
        self.data = data
        self.n_chunks = len(self.data)
        
    def __len__(self):
        return self.n_chunks
        
    def __getitem__(self, idx):
        return self.data[idx]
            
        
class Encoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim=50, hidden_size=50, num_layers=1):
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
    
    def forward(self, x):
        x = self.emb(x)
        _, h = self.lstm(x)
        
        return h
        
class Decoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim=50, hidden_size=50, num_layers=1):
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
        
        self.activation = torch.tanh
        self.l1 = nn.Linear(embedding_dim, (embedding_dim + num_embeddings)//2)
        self.l2 = nn.Linear((embedding_dim + num_embeddings)//2, num_embeddings)
        
    def forward(self, x, h):
        res = []

        embed = self.emb(x)
        
        output, _ = self.lstm(embed, h)
        o = self.l2(self.activation(self.l1(torch.squeeze(output, dim=1))))
        res.append(o)
        
        return o
    
    def generate(self, start_tensor, encoder_hidden, length): 

        res = []

        embed = self.emb(start_tensor)
        embed = embed.unsqueeze(0)

        hidden = encoder_hidden
        
        for _ in range(length):
          output, hidden = self.lstm(embed, hidden)
          o = self.l2(self.activation(self.l1(torch.squeeze(output, dim=1))))
          top_i = torch.argmax(o)  
          res.append(top_i.item())
          embed = self.emb(top_i)
          embed = embed.unsqueeze(0)
          embed = embed.unsqueeze(0)

        print(res)
        return res
        
   
def generate_seq(encoder,decoder, encoder_input, length, temperature=0.8, device="cpu"):
    
    #モデルを評価モードにする
    encoder.eval()
    decoder.eval()
    #出力の数値を格納するリスト
    result = []

    start_word = ["<S>"]
    start_tensor = torch.tensor(str2ints(start_word, vocab_dict), dtype=torch.int64).to(device)
    
    #RNNに通して出力と新しい内部状態を得る
    h = encoder(encoder_input)
    pred_id = decoder.generate(start_tensor, h, length)
    
    
    pred_word = ints2str(pred_id, allwords)
    
    return pred_word
    
        
if __name__=='__main__':
     
    path = "text.txt"


    #前処理
    text = open(path, "r", encoding="utf-8")
    text = text.read()

    text = text.replace('《','\n')
    text = text.replace('》','\n')
    text = text.replace('[','\n')
    text = text.replace(']','\n')
    text = text.replace('#','\n')
    text = text.replace('―','\n')
    text = text.replace('＊','\n')
    text = text.replace('（','\n')
    text = text.replace('）','\n')
    text = text.replace('「','\n')
    text = text.replace('」','\n')
    text = text.replace('［','\n')
    text = text.replace('］','\n')

    text = text.replace('\n',' ')
    words = text.split(' ')
            
            
    #辞書の作成
    allwords = set(words)
    allwords = list(allwords)
    #print(allwords)
    vocab_size = len(allwords)
    vocab_dict = dict((c, i) for (i, c) in enumerate(allwords))
    #print(vocab_dict) 
     
    ds = Dataset(path, chunk_size=50)

    loader = DataLoader(ds, batch_size=32, shuffle=True, num_workers=4)


    encoder = Encoder(vocab_size, 50, 50, num_layers=1)
    decoder = Decoder(vocab_size, 50, 50, num_layers=1)


    #net = SequenceGenerationNet(vocab_size, 50, 50, num_layers=1, dropout=0.2)

    #net.to("cuda:0")


    encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=1.0e-5)
    decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=1.0e-5)


    #opt = optim.SGD(net.parameters(), lr=0.1)


    loss_f = nn.CrossEntropyLoss()

    for epoch in range(1000):

        encoder.train()
        decoder.train()
        losses = []
        for data in tqdm.tqdm(loader):
    
            #Learning
            # x: Input text
            # y: Output text
    
            #xは始めから最後の手前の文字まで
            x = data[:, :-1]
        
            #yは2文字目から最後の文字まで
            y = data[:, 1:]
        
            #x = x.to("cuda:0")
            #y = y.to("cuda:0")
        
            h = encoder(x)
            y_pred = decoder(y, h)
        
            #batchとstepの軸を統合してからCrossEntropyLossに渡す
            loss = loss_f(y_pred.reshape(-1, vocab_size), y.reshape(-1))
            encoder.zero_grad()
            decoder.zero_grad()
            loss.backward()
        
        
            encoder_optimizer.step()
            decoder_optimizer.step()
        
        
            #opt.step()
            losses.append(loss.item())
            
            
            
            
            
            #print(length)

        encoder_input = data[0]
        encoder_input = encoder_input.unsqueeze(0)
        length = 10
        
        #現在の損失関数と生成される文章の例を表示    
        print(epoch, mean(losses))
        with torch.no_grad():
            print(generate_seq(encoder,decoder, encoder_input, length))