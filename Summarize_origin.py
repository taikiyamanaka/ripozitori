import numpy as np
import torch
import matplotlib.pyplot as plt
from torch import nn, optim
import string
from torch.utils.data import(Dataset, DataLoader, TensorDataset)
import tqdm
from statistics import mean
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize



#文字列を数値のリストに変換する関数             
def str2ints(s, vocab_dict):
    return [vocab_dict.get(c, 1) for c in s]

#数値のリストを文字列に変換する関数    
def ints2str(x, allwords):
    return [allwords[i] for i in x]
    
class Dataset(Dataset):
    def __init__(self, texts, allwords, chunk_size):
        
        #ファイルから読み込んだ文章を数値のリストに変換する
        data = []
        
        for i in range(len(texts)):
            #先にテンソルに直す
            tmp = str2ints(texts[i], vocab_dict)
            #print(tmp)
            data.append(torch.tensor(tmp, dtype=torch.int64))
       
        #print(data)
 
        #for i in range(len(texts)):
        
            #A = ints2str(data[i], allwords)
            #X = []
            #for j in range(len(A)):
                #X.append(A[j])
                #X.append(" ")

            #print("".join(X))
       
        
        #最後のchunkの長さをチェックして足りない場合には捨てる
        #if len(data[-1]) < chunk_size:
            #data = data[:-1]
            
        self.data = data
        self.n_chunks = len(self.data)
        
    def __len__(self):
        return self.n_chunks
        
    def __getitem__(self, idx):
        return self.data[idx]
            
        
class Encoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim=50, hidden_size=50, num_layers=1):
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
    
    def forward(self, x):
        x = self.emb(x)
        _, h = self.lstm(x)
        
        return h
        
class Decoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim=50, hidden_size=50, num_layers=1):
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
        
        self.activation = torch.tanh
        self.l1 = nn.Linear(embedding_dim, (embedding_dim + num_embeddings)//2)
        self.l2 = nn.Linear((embedding_dim + num_embeddings)//2, num_embeddings)
        
    def forward(self, x, h):
        res = []

        embed = self.emb(x)
        
        output, _ = self.lstm(embed, h)
        o = self.l2(self.activation(self.l1(torch.squeeze(output, dim=1))))
        res.append(o)
        
        return o
    
    def generate(self, start_tensor, encoder_hidden, length): 

        res = []

        embed = self.emb(start_tensor)
        embed = embed.unsqueeze(0)

        hidden = encoder_hidden
        
        for _ in range(length):
          output, hidden = self.lstm(embed, hidden)
          o = self.l2(self.activation(self.l1(torch.squeeze(output, dim=1))))
          top_i = torch.argmax(o)  
          res.append(top_i.item())
          embed = self.emb(top_i)
          embed = embed.unsqueeze(0)
          embed = embed.unsqueeze(0)

        #print(res)
        return res
        
   
def generate_seq(encoder,decoder, encoder_input, length, temperature=0.8, device="cpu"):
    
    #モデルを評価モードにする
    encoder.eval()
    decoder.eval()
    #出力の数値を格納するリスト
    result = []

    start_word = ["dean"]
    start_tensor = torch.tensor(str2ints(start_word, vocab_dict), dtype=torch.int64).to(device)
    
    #RNNに通して出力と新しい内部状態を得る
    h = encoder(encoder_input)
    pred_id = decoder.generate(start_tensor, h, length)
    
    
    pred_word = ints2str(pred_id, allwords)
    
    return pred_word
    
        
if __name__=='__main__':
     
    path = "sum_dataset/stories/0a0a4c90d59df9e36ffec4ba306b4f20f3ba4acb.story"


    #前処理
    f = open(path, "r", encoding="utf-8")
    #text = text.read()
    
    texts = []
    
    stop = 0

    for rows in f:
        if rows == "\n":
            continue
        if stop == 1:
            highlight = rows
            break
        words = nltk.word_tokenize(rows)

        if rows == "@highlight\n":
            stop = 1
        texts.append(words)
    

    
    f = open(path, "r", encoding="utf-8")
    
    vocab_dict = {}
    index = 0    
    for line in f:
       tmp = sent_tokenize(line)
       if len(tmp) == 0:
           continue
       for sentence in tmp:
           words = word_tokenize(sentence)
           for word in words:
               if word.lower() not in vocab_dict:
                   vocab_dict[word.lower()] = index
                   index += 1
    
    #print(vocab_dict)
    #print(texts)
    
    #print(texts)
    #print("highlight is")
    #print(highlight)
    
    #textsを1次元化
    allwords = sum(texts, [])
    allwords.remove("@")
    allwords.remove("highlight")
    #allwords.insert(0, "")
    #allwords.insert(1, "<NONE>")
    #print(allwords)  
    
    #辞書の作成
    #vocab_dict = dict((c, i) for (i, c) in enumerate(allwords))
    vocab_size = len(allwords)
    #print(vocab_dict)
    
    highlight_low = []
    highlight = word_tokenize(highlight)
    for s in highlight:
       highlight_low.append(s.lower()) 
    #print(highlight_low)
    highlight_id = str2ints(highlight_low, vocab_dict)
    highlight_tensor = torch.tensor(highlight_id, dtype=torch.int64)
    #print(highlight_tensor)
     
    ds = Dataset(texts, allwords, chunk_size=50)
    #print(ds.data)
    #loader = DataLoader(ds, batch_size=32, shuffle=True, num_workers=4)


    encoder = Encoder(vocab_size, 50, 50, num_layers=1)
    decoder = Decoder(vocab_size, 50, 50, num_layers=1)


    #net = SequenceGenerationNet(vocab_size, 50, 50, num_layers=1, dropout=0.2)

    #net.to("cuda:0")


    encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=1.0e-5)
    decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=1.0e-5)
    #print(highlight)

    #opt = optim.SGD(net.parameters(), lr=0.1)


    loss_f = nn.CrossEntropyLoss()

    for epoch in range(10):

        encoder.train()
        decoder.train()
        losses = []
        for data in ds.data:
            #print(data)
            #Learning
            # x: Input text
            # y: Output text
    
            #xは始めから最後の手前の文字まで
            x = data.unsqueeze(0)
        
            #yは2文字目から最後の文字まで
            y = highlight_tensor.unsqueeze(0)
        
            #x = x.to("cuda:0")
            #y = y.to("cuda:0")
        
            h = encoder(x)
            y_pred = decoder(y, h)
            #print(y_pred.reshape(-1, vocab_size))
            #print(y.reshape(-1))
        
            #batchとstepの軸を統合してからCrossEntropyLossに渡す
            loss = loss_f(y_pred.reshape(-1, vocab_size), y.reshape(-1))
            encoder.zero_grad()
            decoder.zero_grad()
            loss.backward()
        
        
            encoder_optimizer.step()
            decoder_optimizer.step()
        
        
            #opt.step()
            losses.append(loss.item())
            
        print(epoch, mean(losses))
            
            
            
            #print(length)

    encoder_input = str2ints(allwords, vocab_dict)
    #print(encoder_input)
    encoder_input = torch.tensor(encoder_input, dtype=torch.int64)
    encoder_input = encoder_input.unsqueeze(0)
    length = 10
        
    #現在の損失関数と生成される文章の例を表示    
    
    with torch.no_grad():
        print(generate_seq(encoder,decoder, encoder_input, length))
        
        