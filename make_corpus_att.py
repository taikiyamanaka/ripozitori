from math import radians
from re import S
import nltk, torch, random, pickle
import matplotlib.pyplot as plt

def check_subject(text, summary):
    #形態素解析
    subject_list = []
    #for summary in su:
    tmp_summary = []
    for word in summary:
        tmp_summary.append(summary_vocab_dict_swap[word.item()])

    #print(nltk.pos_tag(tmp_summary))
    #要約文から主語を獲得
    count = 0
    for tag in nltk.pos_tag(tmp_summary):
        count += 1
        #5単語以内のみ対象とする
        if (count <= 5):
            if((len(tag[0]) != 1 and tag[0] != '<UNK>' and tag[0] != '') 
            and (tag[1] == "NNS" or tag[1] == "NN" or tag[1] == "NNP" or tag[1] == "NNPS")):     
                subject_list.append(tag[0])
                break
        else:
            subject_list.append(-1)
            break
        
    del tmp_summary
        
    #print(subject_list)
    #記事から主語を検索
    idx_list = -1
    for subject in subject_list:
        if (subject == -1): break
        idx = 0
        for word in text:
            if text_vocab_dict_swap[word.item()] == subject:
                idx_list = idx                   
                break
            if idx == len(text) - 1: #本文中に見つからなかった場合
                break
            idx += 1
    #記事の何番目に単語があるか
    #print(idx_list)
    return  idx_list

with open('text_vocab_size_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_size = pickle.load(f)
with open('summary_vocab_size_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_size = pickle.load(f)
with open('text_vocab_dict_swap_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict_swap = pickle.load(f)
with open('summary_vocab_dict_swap_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict_swap = pickle.load(f)
with open('corpus_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    corpus = pickle.load(f)  

print(text_vocab_size,"text size")
print(summary_vocab_size,"sum size")

corpus_idx_sum_att = []
corpus_text = []
i = 0

for train in corpus:
    flag = 0   
    for summary in train[1]:
        idx_list = check_subject(train[0], summary[1:-1])
        if idx_list > -1:
            corpus_idx_sum_att.append([i, summary, idx_list]) #[テキスト, 検索で見つかった単語を持つ要約文, インデックスリスト]
            flag = 1
    if flag == 1:
        corpus_text.append(train[0])
        i += 1
print("text", len(corpus_text))
print("sum",len(corpus_idx_sum_att))
with open('corpus_idx_sum_att5.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(corpus_idx_sum_att, f)                   # オブジェクトをシリアライズ  
with open('corpus_text5.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(corpus_text, f)                   # オブジェクトをシリアライズ  