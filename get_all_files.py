import glob
import os
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize

filename = glob.glob('sum_dataset/stories/*.story')
#filename = ["sum_dataset/stories/0a0a4c90d59df9e36ffec4ba306b4f20f3ba4acb.story", "sum_dataset/stories/0a0aa464d262b903f44b0f8eaa67f13dd1946cfd.story"]
#filename = ["sum_dataset/stories/0abf4bacd3194af1f0c8f254489fddb925d0f4de.story", "sum_dataset/stories/0a0aa464d262b903f44b0f8eaa67f13dd1946cfd.story"]

corpus = []
summary_1 = []
summary_2 = []
summary_3 = []
summary_4 = []
texts = []

#vocab_dict = {"": 0, "UNK": 1, "<S>": 2, "</S>": 3}
index = 4

for file in filename:
    path = file
    
    f = open(path, "r", encoding="utf-8")
    
    tmp_texts = []
    tmp_summary = ["NULL","NULL","NULL","NULL"]
    
    stop = 0
    count = 0
    switch = 1
    for line in f:
        tmp = sent_tokenize(line)

        #空行はスキップ
        if len(tmp) == 0:
            continue  
    
        #要約文のところでストップ
        if stop == 1:
            summary_sent = []
            for sentence in tmp:
                #print(sentence)
                words = word_tokenize(sentence)
                
                #小文字に直して一時的にリストに追加
                for word in words:
                    summary_sent.append(word.lower())
            #要約文ごとにリストに追加
            if count == 4:
                tmp_summary[3] = summary_sent
                break
            if count == 3:
                tmp_summary[2] = summary_sent
                stop = 0
            if count == 2:
                tmp_summary[1] = summary_sent
                stop = 0
            if count == 1:
                tmp_summary[0] = summary_sent
                stop = 0
            

        for sentence in tmp:
            words = word_tokenize(sentence)
            
            #要約文がきたら認識してスキップ
            if sentence == "@highlight":
                
                stop = 1
                switch = 0
                count += 1
                continue
            
            #1記事を1つのテキストとして全て小文字にしてリストに追加
            if switch == 1:
                for word in words:
                    tmp_texts.append(word.lower())
                #辞書作成
                #if word.lower() not in vocab_dict:
                    #vocab_dict[word.lower()] = index
                    #index += 1
                    
    texts.append(tmp_texts)
    summary_1.append(tmp_summary[0])
    summary_2.append(tmp_summary[1])
    summary_3.append(tmp_summary[2])
    summary_4.append(tmp_summary[3])   
        
#print(summary_1)  
#print(summary_2) 
#print(summary_3) 
#print(summary_4) 
#print(texts)
corpus = [(x, y1, y2, y3, y4) for x, y1, y2, y3, y4 in zip(texts, summary_1,summary_2,summary_3,summary_4)]  
#print(corpus)


new_file = open("corpus2.txt",'w')

for x in corpus:
    
    if x[0] != "NULL":
        str1 = " ".join(x[0])
    else:
        str1 = "NULL"
        
    if x[1] != "NULL":    
        str2 = " ".join(x[1])
    else:
        str2 = "NULL"  
  
    if x[2] != "NULL": 
        str3 = " ".join(x[2])
    else:
        str3 = "NULL" 
        
    if x[3] != "NULL":     
        str4 = " ".join(x[3])
    else:
        str4 = "NULL" 
        
    if x[4] != "NULL":
        str5 = " ".join(x[4])
    else:
        str5 = "NULL"

    new_file.writelines(str1)
    new_file.writelines("\t")
    new_file.writelines(str2)
    new_file.writelines("\t")
    new_file.writelines(str3)
    new_file.writelines("\t")
    new_file.writelines(str4)
    new_file.writelines("\t")
    new_file.writelines(str5)
    new_file.writelines("\n")
    
new_file.close()

