from math import radians
from re import S
import nltk, torch, model_v1, random, pickle
import matplotlib.pyplot as plt

def make_att_train(batch_data, corpus_t, encoder, decoder, encoder_optimizer, decoder_optimizer):
    loss = 0.0
    loss_count = 0
    for i, su, idx_list in batch_data:      
        att_w0 = []
        hs, h = encoder(corpus_t[i])
        for j in range(len(corpus_t[i])):
            if j == idx_list:
                att_w0.append(1)
            else:
                att_w0.append(0)
        pred = decoder(su[:-1], hs, h, torch.Tensor(att_w0))
        del att_w0

        loss += criterion(pred, su[1:])
        loss_count += 1
        print("Loss count:", loss_count)
        print(loss)

    encoder.zero_grad()
    decoder.zero_grad()
    loss /= loss_count
    loss.backward()
    #loss = 0.0
    encoder_optimizer.step()
    decoder_optimizer.step()
    return loss.item()

with open('text_vocab_size_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_size = pickle.load(f)
with open('summary_vocab_size_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_size = pickle.load(f)
with open('text_vocab_dict_swap_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict_swap = pickle.load(f)
with open('summary_vocab_dict_swap_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict_swap = pickle.load(f)
with open('corpus_text5.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    corpus_t = pickle.load(f)  
with open('corpus_idx_sum_att5.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    corpus_s = pickle.load(f)  

print(text_vocab_size,"text size")
print(summary_vocab_size,"sum size")

encoder = model_v1.Encoder(text_vocab_size, 256, 256)
decoder = model_v1.Decoder(summary_vocab_size, 256, 256)
print("MODEL CREATED.")

encoder_optimizer = torch.optim.Adam(encoder.parameters())
decoder_optimizer = torch.optim.Adam(decoder.parameters())

criterion = torch.nn.CrossEntropyLoss()

loss_history = []

for epoch in range(100):
    
    randomize_corpus = random.sample(corpus_s, len(corpus_s))
    print("corpus_s", len(corpus_s))
    count = 0
    b_count = 0
    batch_data = []
    epoch_loss = 0

    for train in randomize_corpus:   
        count += 1
        #print("Total:", count)
        batch_data.append(train)
        b_count += 1
        #print("Batch:", b_count)
        if b_count == 256:
            print("len:", len(batch_data))
            epoch_loss += make_att_train(batch_data, corpus_t, encoder, decoder, encoder_optimizer, decoder_optimizer)
            b_count = 0
            batch_data = []
            #print('Middle_epoch_loss:', epoch_loss)

    #256未満の時の処理        
    if len(batch_data) > 0:
        epoch_loss += make_att_train(batch_data, encoder, decoder, encoder_optimizer, decoder_optimizer)
        batch_data = []
        #print('Middle_epoch_loss:', epoch_loss)
    
    #print(corpus[0][1][0][0:1]) # <S>を入れている 
    
    print('Epoch:', epoch+1)
    print('Total_epoch_loss:', epoch_loss)
    loss_history.append(epoch_loss)

    # n個目の記事を検証
    n = 0
    
    for i in range(len(corpus[n][1])):
        print('Original: {}'.format(' '.join([summary_vocab_dict_swap[x.item()] for x in corpus[n][1][i][1:-1]])))
    hs, h = encoder(corpus[n][0])

    # 要約文の中で一番単語数が多い要約文の長さを抽出
    i = -1
    max = 0
    for sum in corpus[n][1]:
        i += 1
        if len(sum) > max:
            idx = i
    
    subject_list_tmp = []
    #形態素解析
    for summary in corpus[n][1]:
        tmp_summary = []
        
        for word in summary:
            tmp_summary.append(summary_vocab_dict_swap[word.item()])
            
        #要約文から主語を獲得
        for tag in nltk.pos_tag(tmp_summary):
                #print(tag)
            if((tag[1] == "NNS" or tag[1] == "NN") and tag[0] != "<S>"):     
                subject_list_tmp.append(tag[0])
                break
    print(subject_list_tmp)
    
    # 出力させたい単語を指定
    subject_0 = ["higgs"]
    
    idx_list2 = []
    #記事から主語を検索
    for subject in subject_0:
        idx2 = 0
        for word in corpus[n][0]:
            if text_vocab_dict_swap[word.item()] == subject:
                idx_list2.append(idx2)
                print(subject, "が見つかりました。この単語にAttentionをかけて生成します。")                 
                break
            if idx2 == len(corpus[n][0]) - 1:
                idx_list2.append(-1)
                print(subject, "が見つかりませんでした。")
                break
            idx2 += 1
    print(idx_list2)
    
    att_w02 = []
    
    # 何番目の要約文を出現させたいか
    sum_num = 0

    #print(subject_list_tmp[sum_num],"を主語として生成します。")       
    for j in range(len(corpus[n][0])):
        #print(len(train[0]),"train[0]")
        if j == idx_list2[sum_num]:
            #print(idx_list[i])
            att_w02.append(1)
        else:
            att_w02.append(0)
    
    #text内容を表示
    #print('{}'.format(' '.join([text_vocab_dict_swap[x.item()] for x in corpus[n][0]])))
    #print(torch.Tensor(att_w02).size(),"att_w02")
    pred = decoder.predict(corpus[n][1][0][0:1], hs, h, len(corpus[n][1][idx]) - 1, torch.Tensor(att_w02))
    print('Predict: {}'.format(' '.join([summary_vocab_dict_swap[x] for x in pred])))
    
    #if loss.item() < 0.1: break

with open('encoder_v2.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(encoder, f)                   # オブジェクトをシリアライズ  
with open('decoder_v2.pickle', mode='wb') as g:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(decoder, g)                   # オブジェクトをシリアライズ      
with open('loss_history_v2.pickle', mode='wb') as g:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(loss_history, g)                   # オブジェクトをシリアライズ   
#plt.plot(loss_history)
#plt.show()
