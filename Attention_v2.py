import torch
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from torch import nn, optim
from statistics import mean
import glob
import numpy as np

#-------------------------------------------------------------------------------
#関数定義

#文字列を数値のリストに変換する関数             
def str2ints(s, vocab_dict):
    return [vocab_dict.get(c, 1) for c in s]

#数値のリストを文字列に変換する関数    
def ints2str(x, vocab_dict_swap):
    return [vocab_dict_swap.get(i, 1) for i in x]
    
#リストをテンソルに変換する関数
def totensor(x):
    return torch.tensor(x, dtype=torch.int64)
    
class Encoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, hidden_size, num_layers=1):
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
        
        #print(num_embeddings) [541]記事辞書単語数
        #print(embedding_dim) [256]
        #print(hidden_size)[256]
    
    def forward(self, x):
        x = self.emb(x)
        hs, h = self.lstm(x)
        
        return hs, h
        
class Decoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, hidden_size, num_layers=1):
    
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
        
        self.hidden2linear = nn.Linear(hidden_size * 2, num_embeddings)
        
        self.activation = torch.tanh
        #self.l1 = nn.Linear(embedding_dim, (embedding_dim + num_embeddings)//2)
        #self.l2 = nn.Linear((embedding_dim + num_embeddings)//2, num_embeddings)
        
        self.softmax = nn.Softmax(dim=1)
        self.hidden_size = hidden_size
        
        #print(num_embeddings) [30]要約辞書単語数
        #print(embedding_dim) [256]
        #print(hidden_size) [256]
        
    def forward(self, x, hs, init_hidden, attention_weight_1_tensor):
    
        init_c = torch.zeros_like(init_hidden[1])
        embed = self.emb(x)
        output, state = self.lstm(embed, (init_hidden[0], init_c))
        
        #Attention層
        #hs.size() = [1,572,256] 記事の単語数
        #output.size() = [1,14,256] 要約文の単語数
        
        #print(hs[0][22])
        
        #print(hs.size())
        #print(output.size())
        
        #bmmを使ってEncoder側の出力(hs)とDecoder側の出力(output)を
        #batchごとまとめて行列計算するために、Decoder側のoutputをbatchを固定して転置行列を取る
        t_output = torch.transpose(output, 1, 2)
        
        #bmmでバッチも考慮してまとめて行列計算
        s = torch.bmm(hs, t_output)
        
        #列方向(dim=1)でsoftmaxをとって確率表現に変換
        #この値を後のAttentionの可視化などにも使うため、returnで返しておく
        attention_weight = self.softmax(s)
        
        #コンテキストベクトルをまとめるために入れ物を用意
        c = torch.zeros(1, 1, self.hidden_size)
        
        #print(range(attention_weight.size()[2]))
        
        #各層（Decoder側のlstm層は生成文字列がn文字なのでn個ある）におけるattention weightを
        #取り出してforループ内でコンテキストベクトルを１つずつ作成する
        for i in range(attention_weight.size()[2]):
        
            #初期Attentionを当てる
            if i == 0:
                weighted_hs = hs * attention_weight_1_tensor
                weight_sum = torch.sum(weighted_hs, axis=1).unsqueeze(1)
                #print(weight_sum.size())
                c = torch.cat([c, weight_sum], dim=1)
            else:
                #i番目のLSTM層に対するattention weightを取り出すが、テンソルのサイズをhsと揃えるためにunsqueezeする
                unsq_weight = attention_weight[:,:,i].unsqueeze(2)
            
                #hsの各ベクトルをattention weightで重み付けする
                weighted_hs = hs * unsq_weight
                #print(hs.size())
                #attention weightで重み付けされた各hsのベクトルをすべて足し合わせてコンテキストベクトルを作成
                weight_sum = torch.sum(weighted_hs, axis=1).unsqueeze(1)
                
            
                c = torch.cat([c, weight_sum], dim=1)
        
        #箱として用意したzero要素が残っているのでスライスして削除
        c = c[:,1:,:]
        
        output = torch.cat([output, c], dim=2)
        o = self.hidden2linear(output)
        
        return o, state, attention_weight
    
    def generate(self, start_tensor, decoder_hidden, hs, length, attention_weight_1_tensor): 

        res = []

        embed = self.emb(start_tensor)
        embed = embed.unsqueeze(0)
        #print(embed)

        hidden = decoder_hidden
        
        for _ in range(length):
          output, hidden = self.lstm(embed, hidden)
          t_output = torch.transpose(output, 1, 2)
          s = torch.bmm(hs, t_output)
          attention_weight = self.softmax(s)
          c = torch.zeros(1, 1, self.hidden_size)
          for i in range(attention_weight.size()[2]):
            #初期Attentionを当てる
            if i == 0:
                weighted_hs = hs * attention_weight_1_tensor
                weight_sum = torch.sum(weighted_hs, axis=1).unsqueeze(1)
                #print(weight_sum.size())
                c = torch.cat([c, weight_sum], dim=1)
            else:
                unsq_weight = attention_weight[:,:,i].unsqueeze(2)
                weighted_hs = hs * unsq_weight
                weight_sum = torch.sum(weighted_hs, axis=1).unsqueeze(1)
                c = torch.cat([c, weight_sum], dim=1)
          c = c[:,1:,:]  
          output = torch.cat([output, c], dim=2)  
          
          o = self.hidden2linear(output)
          top_i = torch.argmax(o)
          #print(top_i)  
          res.append(top_i.item())
          embed = self.emb(top_i)
          embed = embed.unsqueeze(0)
          embed = embed.unsqueeze(0)

        return res
        
def generate_seq(encoder, decoder, encoder_input, length, vocab_dict, idx_list, length_text_vocab):
    
    #出力の数値を格納するリスト
    result = []

    start_word = ["<S>"]
    start_tensor = totensor(str2ints(start_word, vocab_dict))
    
    #RNNに通して出力と新しい内部状態を得る
    hs, encoder_state = encoder(encoder_input)
    
    attention_weight_1 = []
        
    for k in range(length_text_vocab):
        if k == idx_list[i]:
            attention_weight_1.append(1)
        else:
            attention_weight_1.append(0)
        
    attention_weight_1_tensor = totensor(attention_weight_1)
        
    #次元を合わす
    attention_weight_1_tensor = attention_weight_1_tensor.unsqueeze(0)
    attention_weight_1_tensor = attention_weight_1_tensor.unsqueeze(0)
    #転置
    attention_weight_1_tensor = torch.transpose(attention_weight_1_tensor, 1, 2)


    #decoder_output, _, attention_weight = decoder(y, hs, h, attention_weight_1_tensor)
    
    decoder_hidden = encoder_state
    
    
    pred_id = decoder.generate(start_tensor, decoder_hidden, hs, length, attention_weight_1_tensor)
       
    pred_word = ints2str(pred_id, vocab_dict_summary_swap)
    pred_word = "".join([word + " " for word in pred_word])
    
    return pred_word

#-------------------------------------------------------------------------------
#前処理

#辞書作成
corpus = []
summary = []
texts = []

count = 0

vocab_dict = {"": 0, "UNK": 1, "<S>": 2, "</S>": 3}
index = 4

f = open("corpus.txt", "r", encoding="utf-8")

for lines in f:

    line = lines.rstrip('\n').split('\t')
    
    tmp_texts = line[0]
    tmp_texts = nltk.word_tokenize(tmp_texts)
    tmp_summary = line[1]
    tmp_summary = nltk.word_tokenize(tmp_summary)
    
    texts.append(["<S>"] + tmp_texts + ["</S>"])
    summary.append(["<S>"] + tmp_summary + ["</S>"])
    
    for word in tmp_texts:
        if word not in vocab_dict:
            vocab_dict[word.lower()] = index
            index += 1
    
    #count + 1 の記事が反映
    if count == 10:
        break
    count += 1

f.close()
    
corpus = [(x, y) for x, y in zip(texts, summary)]
#print(corpus[50][1])
#print(vocab_dict)
#print(corpus[1][0])     

#逆順辞書作成                
vocab_dict_swap = {v: k for k, v in vocab_dict.items()}
vocab_size = len(vocab_dict)

#print(vocab_size)               
#print(texts)
#print(summary)
#print(vocab_dict)
#print(vocab_dict_swap)

summary_tag = []
#形態素解析
for sentence in summary:
    summary_tag.append(nltk.pos_tag(sentence))
    
#print(summary_tag)    

#要約文から主語を獲得    
subject_list = []

for tag in summary_tag:
    for t in tag:
        if((t[1] == "NNS" or t[1] == "NN") and t[0] != "<S>"):     
            subject_list.append(t[0])
            break
#各要約文の主語の1単語のリスト
#print(subject_list)

subject_id_list = []

#エンコーダ辞書を用いてIDへ変換
subject_id_list = str2ints(subject_list, vocab_dict)
#print(ints2str(subject_id_list, vocab_dict_swap)) 

idx_list = []
#記事から主語を検索
for (subject, text) in zip(subject_list, texts):
    flag = 0
    idx = 0
    #print(subject)
    #print(text)
    if flag == 0:
        for word in text:
            if word == subject:
                idx_list.append(idx)                   
                flag = 1
                break
            if word == "</S>":
                idx_list.append(-1)
                flag = 1
                break
            idx += 1
#記事の何番目に単語があるか
#print(idx_list)            
    
#デコーダ用辞書の作成
vocab_dict_summary = {"": 0, "UNK": 1, "<S>": 2, "</S>": 3}

index = 4

for sentence in summary:
    for word in sentence:
        if word not in vocab_dict_summary:
            vocab_dict_summary[word] = index
            index += 1  
        
#print(vocab_dict_summary)

#デコーダ用逆順辞書作成                
vocab_dict_summary_swap = {v: k for k, v in vocab_dict_summary.items()}
vocab_size_summary = len(vocab_dict_summary)

#print(vocab_dict_summary_swap)
#print(vocab_size_summary)

#ファイルから読み込んだ文章を数値のリスト、テンソルに変換する
data = []
for text in corpus:
    data.append(totensor(str2ints(text[0], vocab_dict)))
  
summary_tensor = []
for sum in corpus:
    summary_tensor.append(totensor(str2ints(sum[1], vocab_dict_summary)))
       
encoder = Encoder(vocab_size, 256, 256)
decoder = Decoder(vocab_size_summary, 256, 256)

encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=0.01)
decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=0.01)

loss_f = nn.CrossEntropyLoss()

#-------------------------------------------------------------------------------
#学習

all_losses = []

for epoch in range(100):
    epoch_loss = 0
    for i in range(len(texts)):
        x = data[i][1:-1]
        #z = x.tolist()
        #print(z)
        #print(ints2str(z, vocab_dict_swap))
        x = x.unsqueeze(0)
        y = summary_tensor[i][:-1]
        #z_2 = y.tolist()
        #print(y)
        #print(ints2str(z_2, vocab_dict_summary_swap))
        y = y.unsqueeze(0)
        y_t = summary_tensor[i][1:]
        y_t = y_t.unsqueeze(0)
        hs, h = encoder(x)
        
        #print(hs)
        attention_weight_1 = []
        
        for k in range(len(data[i][1:-1])):
            if k == idx_list[i]:
                attention_weight_1.append(1)
            else:
                attention_weight_1.append(0)
        
        attention_weight_1_tensor = totensor(attention_weight_1)
        
        #次元を合わす
        attention_weight_1_tensor = attention_weight_1_tensor.unsqueeze(0)
        attention_weight_1_tensor = attention_weight_1_tensor.unsqueeze(0)
        #転置
        attention_weight_1_tensor = torch.transpose(attention_weight_1_tensor, 1, 2)
        
        #print(attention_weight_1_tensor.size())

        decoder_output, _, attention_weight = decoder(y, hs, h, attention_weight_1_tensor)
        
        loss = 0
        #print(decoder_output.size()[1])
        for j in range(decoder_output.size()[1]):
            loss += loss_f(decoder_output[:, j, :], y_t[:, j])
    
        epoch_loss += loss.item()
        
        
        #res = []
    
        #for word_p in y_pred:
            #top_i = torch.argmax(word_p)
            #res.append(top_i.item())
        #print(res)
        #pred_word = ints2str(res, vocab_dict_summary_swap)
        #print(pred_word)
    
        #loss = loss_f(y_pred.reshape(-1, vocab_size_summary), y_t.reshape(-1))
        encoder.zero_grad()
        decoder.zero_grad()
        loss.backward()
    
        encoder_optimizer.step()
        decoder_optimizer.step()

        #losses.append(loss.item())
    
    #print(epoch, mean(losses))
    print("Epoch %d: %.2f" % (epoch, epoch_loss))
    all_losses.append(epoch_loss)
    if epoch_loss < 0.1: break
    
    for k in range(count):
        encoder_input = data[k][1:-1]
        #print(ints2str(encoder_input.tolist(), vocab_dict_swap))
        encoder_input = encoder_input.unsqueeze(0)
        length = 14
        length_text_vocab = len(data[k][1:-1])

        #現在の損失関数と生成される文章の例を表示    
    
        with torch.no_grad():
            print(generate_seq(encoder, decoder, encoder_input, length, vocab_dict, idx_list, length_text_vocab))
    
    
#-------------------------------------------------------------------------------
#最終予測

#encoder_input = data[0]
#print(encoder_input)
#encoder_input = encoder_input.unsqueeze(0)
#length = 14
   
#with torch.no_grad():
    #print(generate_seq(encoder, decoder, encoder_input, length, vocab_dict))
        
        