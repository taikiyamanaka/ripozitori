﻿import torch
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from torch import nn, optim
from statistics import mean
import glob

#-------------------------------------------------------------------------------
#関数定義

#文字列を数値のリストに変換する関数             
def str2ints(s, vocab_dict):
    return [vocab_dict.get(c, 1) for c in s]

#数値のリストを文字列に変換する関数    
def ints2str(x, vocab_dict_swap):
    return [vocab_dict_swap.get(i, 1) for i in x]
    
#リストをテンソルに変換する関数
def totensor(x):
    return torch.tensor(x, dtype=torch.int64)
    
class Encoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, hidden_size, num_layers=1):
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
    
    def forward(self, x):
        x = self.emb(x)
        _, h = self.lstm(x)
        
        return h
        
class Decoder(nn.Module):
    def __init__(self, num_embeddings, embedding_dim, hidden_size, num_layers=1):
    
        super().__init__()
        self.emb = nn.Embedding(num_embeddings, embedding_dim)
        self.lstm = nn.LSTM(embedding_dim, hidden_size, num_layers, batch_first=True)
        
        self.activation = torch.tanh
        self.l1 = nn.Linear(embedding_dim, (embedding_dim + num_embeddings)//2)
        self.l2 = nn.Linear((embedding_dim + num_embeddings)//2, num_embeddings)
        
    def forward(self, x, init_hidden):
    
        init_c = torch.zeros_like(init_hidden[1])
        embed = self.emb(x)
        output, _ = self.lstm(embed, (init_hidden[0], init_c))
        o = self.l2(self.activation(self.l1(torch.squeeze(output, dim=0))))
        
        return o
    
    def generate(self, start_tensor, encoder_hidden, length): 

        res = []

        embed = self.emb(start_tensor)
        embed = embed.unsqueeze(0)
        #print(embed)

        hidden = encoder_hidden
        
        for _ in range(length):
          output, hidden = self.lstm(embed, hidden)
          o = self.l2(self.activation(self.l1(torch.squeeze(output, dim=0))))
          top_i = torch.argmax(o)
          #print(top_i)  
          res.append(top_i.item())
          embed = self.emb(top_i)
          embed = embed.unsqueeze(0)
          embed = embed.unsqueeze(0)

        return res
        
def generate_seq(encoder, decoder, encoder_input, length, vocab_dict):
    
    #出力の数値を格納するリスト
    result = []

    start_word = ["<S>"]
    start_tensor = totensor(str2ints(start_word, vocab_dict))
    
    #RNNに通して出力と新しい内部状態を得る
    h = encoder(encoder_input)
    pred_id = decoder.generate(start_tensor, h, length)
       
    pred_word = ints2str(pred_id, vocab_dict_summary_swap)
    pred_word = "".join([word + " " for word in pred_word])
    
    return pred_word

#-------------------------------------------------------------------------------
#前処理

corpus = []
summary = []
texts = []

count = 0

vocab_dict = {"": 0, "UNK": 1, "<S>": 2, "</S>": 3}
index = 4

f = open("corpus.txt", "r", encoding="utf-8")

for lines in f:

    if count == 100:
        break

    line = lines.rstrip('\n').split('\t')
    
    tmp_texts = line[0]
    tmp_texts = nltk.word_tokenize(tmp_texts)
    tmp_summary = line[1]
    tmp_summary = nltk.word_tokenize(tmp_summary)
    
    texts.append(["<S>"] + tmp_texts + ["</S>"])
    summary.append(["<S>"] + tmp_summary + ["</S>"])
    
    for word in tmp_texts:
        if word not in vocab_dict:
            vocab_dict[word.lower()] = index
            index += 1
    
    
    count += 1

f.close()
    
corpus = [(x, y) for x, y in zip(texts, summary)]
#print(corpus[50][1])
#print(vocab_dict)


  
#print(corpus[1][0])     

#逆順辞書作成                
vocab_dict_swap = {v: k for k, v in vocab_dict.items()}
vocab_size = len(vocab_dict)

#print(vocab_size)               
#print(texts)
#print(summary[0])
#print(vocab_dict)
#print(vocab_dict_swap)

#デコーダ用辞書の作成
vocab_dict_summary = {"": 0, "UNK": 1, "<S>": 2, "</S>": 3}

index = 4

for sentence in summary:
    for word in sentence:
        if word not in vocab_dict_summary:
            vocab_dict_summary[word] = index
            index += 1  
        
#print(vocab_dict_summary)

#デコーダ用逆順辞書作成                
vocab_dict_summary_swap = {v: k for k, v in vocab_dict_summary.items()}
vocab_size_summary = len(vocab_dict_summary)

#print(vocab_dict_summary_swap)
#print(vocab_size_summary)

#ファイルから読み込んだ文章を数値のリスト、テンソルに変換する
data = []
for text in corpus:
    data.append(totensor(str2ints(text[0], vocab_dict)))
  
summary_tensor = []
for sum in corpus:
    summary_tensor.append(totensor(str2ints(sum[1], vocab_dict_summary)))
    
encoder = Encoder(vocab_size, 256, 256)
decoder = Decoder(vocab_size_summary, 256, 256)

encoder_optimizer = torch.optim.Adam(encoder.parameters(), lr=0.01)
decoder_optimizer = torch.optim.Adam(decoder.parameters(), lr=0.01)

loss_f = nn.CrossEntropyLoss()

#-------------------------------------------------------------------------------
#学習

all_losses = []

for epoch in range(100):
    loss = 0
    for i in range(len(texts)):
        x = data[i][1:-1]
        x = x.unsqueeze(0)
        y = summary_tensor[i][:-1]
        #print(y)
        y = y.unsqueeze(0)
        y_t = summary_tensor[i][1:]
        y_t = y_t.unsqueeze(0)
        h = encoder(x)
        y_pred = decoder(y, h)
        decoder_output = y_pred.unsqueeze(0)
        
        #res = []
    
        #for word_p in y_pred:
            #top_i = torch.argmax(word_p)
            #res.append(top_i.item())
        #print(res)
        #pred_word = ints2str(res, vocab_dict_summary_swap)
        #print(pred_word)
        
        #loss = 0
        loss += loss_f(y_pred.reshape(-1, vocab_size_summary), y_t.reshape(-1))
        
        
        #for j in range(decoder_output.size()[1]):
            #loss += loss_f(decoder_output[:, j, :], y_t[:, j])
    
        #epoch_loss += loss.item()

        #losses.append(loss.item())
    
    loss /= len(texts)
    #print("Epoch %d: %.2f" % (epoch, epoch_loss))
    print('Epoch', epoch+1, loss.item())
    encoder.zero_grad()
    decoder.zero_grad()
    loss.backward()
    encoder_optimizer.step()
    decoder_optimizer.step()
    
    if loss < 0.1: break
    
    encoder_input = data[0]
    #print(encoder_input)
    encoder_input = encoder_input.unsqueeze(0)
    length = 14

    #現在の損失関数と生成される文章の例を表示    
    
    with torch.no_grad():
        print(generate_seq(encoder, decoder, encoder_input, length, vocab_dict))
    
    
#-------------------------------------------------------------------------------
#予測

encoder_input = data[0]
#print(encoder_input)
encoder_input = encoder_input.unsqueeze(0)
length = 14

#現在の損失関数と生成される文章の例を表示    
    
with torch.no_grad():
    print(generate_seq(encoder, decoder, encoder_input, length, vocab_dict))
        
        