import nltk, torch, model, random, pickle
import matplotlib.pyplot as plt

corpus = []
summaries_all = []
texts = []
count = 0

text_vocab_dict = {'UNK': 0, '<S>': 1, '</S>': 2}
text_index = 3
summary_vocab_dict = {'UNK': 0, '<S>': 1, '</S>': 2}
summary_index = 3

for line in open('corpus2.txt', encoding='utf-8'):
    count += 1
    elements = line.strip().split('\t')
    
    if count == 90001:
        break
    texts.append([x.lower() for x in nltk.word_tokenize(elements[0])])
    #print(elements[4])
    summaries_set = []
    #for i in range(len(elements)-1):
        #print(elements[i+1])

    for i in range(len(elements)-1):
        if elements[i+1] == "NULL":
            break
        else:
            summaries_set.append(["<S>"] + [x.lower() for x in nltk.word_tokenize(elements[i+1])] + ["</S>"])
            
    summaries_all.append(summaries_set)
    
    for word in texts[-1]:
        if word not in text_vocab_dict:
            text_vocab_dict[word] = text_index
            text_index += 1
    for summaries_set in summaries_all:        
        for summary in summaries_set:
            for word in summary:
                #print(word)
                if word not in summary_vocab_dict:
                    summary_vocab_dict[word] = summary_index
                    summary_index += 1

text_vocab_size = len(text_vocab_dict)
summary_vocab_size = len(summary_vocab_dict)
text_vocab_dict_swap = {v: k for k, v in text_vocab_dict.items()}
summary_vocab_dict_swap = {v: k for k, v in summary_vocab_dict.items()}

for t, summaries in zip(texts, summaries_all):
    tmp_summaries = []
    for s in summaries:
        tmp_summaries.append(torch.tensor([summary_vocab_dict.get(x, 0) for x in s]))
    corpus.append((torch.tensor([text_vocab_dict.get(x, 0) for x in t]), tmp_summaries))

# 0記事目の要約文にアクセス
#print(corpus[0][1])
#print(corpus[0])

#del texts, summaries_all
                    
#リストのそれぞれの要素数を確認                    
#for i in range(len(summaries_all)):
    #print(len(summaries_all[i]))
    
#print(text_vocab_dict)           
#print(summary_vocab_dict)
#print(summaries_all)
#print(texts)
#print(text_vocab_size)
#print(summary_vocab_size)
#print(text_vocab_dict_swap)
#print(summary_vocab_dict_swap) 

with open('text_vocab_dict_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(text_vocab_dict, f)                   # オブジェクトをシリアライズ  
with open('summary_vocab_dict_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(summary_vocab_dict, f)                   # オブジェクトをシリアライズ   
with open('text_vocab_size_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(text_vocab_size, f)                   # オブジェクトをシリアライズ       
with open('summary_vocab_size_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(summary_vocab_size, f)                   # オブジェクトをシリアライズ   
with open('text_vocab_dict_swap_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(text_vocab_dict_swap, f)                   # オブジェクトをシリアライズ   
with open('summary_vocab_dict_swap_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(summary_vocab_dict_swap, f)                   # オブジェクトをシリアライズ   
with open('texts_90000.pickle', mode='wb') as f:              # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(texts, f)                               # オブジェクトをシリアライズ   
with open('summaries_all_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(summaries_all, f)                   # オブジェクトをシリアライズ      
with open('corpus_90000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(corpus, f)                   # オブジェクトをシリアライズ      