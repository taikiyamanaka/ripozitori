import nltk, torch, model, random, pickle
import matplotlib.pyplot as plt

with open('text_vocab_dict.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict = pickle.load(f)
with open('summary_vocab_dict.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict = pickle.load(f)
with open('text_vocab_size.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_size = pickle.load(f)
with open('summary_vocab_size.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_size = pickle.load(f)
with open('text_vocab_dict_swap.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict_swap = pickle.load(f)
with open('summary_vocab_dict_swap.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict_swap = pickle.load(f)
with open('texts.pickle', mode='rb') as f:              # with構文でファイルパスとバイナリ書き込みモードを設定
    texts = pickle.load(f)
with open('summaries_all.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summaries_all = pickle.load(f)
#with open('corpus.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
#    corpus = pickle.load(f)   

with open('encoder_1000_200.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    encoder = pickle.load(f)   
with open('decoder_1000_200.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    decoder = pickle.load(f)       
#encoder = model.Encoder(text_vocab_size, 256, 256)
#decoder = model.Decoder(summary_vocab_size, 256, 256)
print(text_vocab_size,"text size")
print(summary_vocab_size,"sum size")

corpus = []
summaries_all = []
texts = []
count = 0

for line in open('corpus2.txt', encoding='utf-8'):
    if count < 1100:
        count += 1
    else:
        elements = line.strip().split('\t')
        texts.append([x.lower() for x in nltk.word_tokenize(elements[0])])
        summaries_set = []
        for i in range(len(elements)-1):
            if elements[i+1] == "NULL":
                break
            else:
                summaries_set.append(["<S>"] + [x.lower() for x in nltk.word_tokenize(elements[i+1])] + ["</S>"])
        summaries_all.append(summaries_set) 
        break       

print(texts)
print(summaries_all)

for t, summaries in zip(texts, summaries_all):
    tmp_summaries = []
    for s in summaries:
        tmp_summaries.append(torch.tensor([summary_vocab_dict.get(x, 0) for x in s]))
    corpus.append((torch.tensor([text_vocab_dict.get(x, 0) for x in t]), tmp_summaries))
                
for epoch in range(1):

    # n個目の記事を検証
    n = 0
    
    for i in range(len(corpus[n][1])):
        print('Original: {}'.format(' '.join([summary_vocab_dict_swap[x.item()] for x in corpus[n][1][i][1:-1]])))
    hs, h = encoder(corpus[n][0])
    #print(hs.size(),"hs")
    #print(corpus[n][0].size(),"corpus[n][0]")
    # 要約文の中で一番単語数が多い要約文の長さを抽出
    i = -1
    max = 0
    for sum in corpus[n][1]:
        i += 1
        if len(sum) > max:
            idx = i
    
    subject_list_tmp = []
    #形態素解析
    for summary in corpus[n][1]:
        tmp_summary = []
        
        for word in summary:
            tmp_summary.append(summary_vocab_dict_swap[word.item()])
            #print(tmp_summary)
            #print(nltk.pos_tag(tmp_summary))
            
        #要約文から主語を獲得
        for tag in nltk.pos_tag(tmp_summary):
                #print(tag)
            if((tag[1] == "NNS" or tag[1] == "NN") and tag[0] != "<S>"):     
                subject_list_tmp.append(tag[0])
                break
    print(subject_list_tmp)
    
    # 出力させたい単語を指定
    subject_0 = ["minister"]
    
    idx_list2 = []
    #記事から主語を検索
    #for subject in subject_list_tmp:
    for subject in subject_0:
        #print(subject)
        idx2 = 0
        #print(subject)
        #print(text_vocab_dict_swap[train[0].item()])
        for word in corpus[n][0]:
                #print(idx)
                #print(len(train[0]))
                #print(text_vocab_dict_swap[word.item()])
            if text_vocab_dict_swap[word.item()] == subject:
                idx_list2.append(idx2)
                print(subject, "が見つかりました。この単語にAttentionをかけて生成します。")                 
                break
            if idx2 == len(corpus[n][0]) - 1:
                idx_list2.append(-1)
                print(subject, "が見つかりませんでした。")
                break
            idx2 += 1
    print(idx_list2)
    
    att_w02 = []
    
    # 何番目の要約文を出現させたいか
    sum_num = 0

    #print(subject_list_tmp[sum_num],"を主語として生成します。")       
    for j in range(len(corpus[n][0])):
        #print(len(train[0]),"train[0]")
        if j == idx_list2[sum_num]:
            #print(idx_list[i])
            att_w02.append(1)
        else:
            att_w02.append(0)
    
    #text内容を表示
    #print('{}'.format(' '.join([text_vocab_dict_swap[x.item()] for x in corpus[n][0]])))
    #print(torch.Tensor(att_w02).size(),"att_w02")
    pred = decoder.predict(corpus[n][1][0][0:1], hs, h, len(corpus[n][1][idx]) - 1, torch.Tensor(att_w02))
    print('Predict: {}'.format(' '.join([summary_vocab_dict_swap[x] for x in pred])))


#with open('encoder_1000.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
#    pickle.dump(encoder, f)                   # オブジェクトをシリアライズ  
#with open('decoder_1000.pickle', mode='wb') as g:  # with構文でファイルパスとバイナリ書き込みモードを設定
#    pickle.dump(decoder, g)                   # オブジェクトをシリアライズ      
 
#plt.plot(loss_history)
#plt.show()
