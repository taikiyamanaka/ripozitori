import torch

class Encoder(torch.nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super().__init__()

        self.embedID = torch.nn.Embedding(vocab_size, embed_size)
        self.lstm = torch.nn.LSTM(embed_size, hidden_size, batch_first=True)

    def forward(self, i):
        x = torch.unsqueeze(self.embedID(i), dim=0)
        return self.lstm(x)

class Decoder(torch.nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size):
        super().__init__()

        self.embedID = torch.nn.Embedding(vocab_size, embed_size)
        self.lstm = torch.nn.LSTM(embed_size, hidden_size, batch_first=True)

        self.l1 = torch.nn.Linear(hidden_size * 2, (hidden_size * 2 + vocab_size) // 2)
        self.l2 = torch.nn.Linear((hidden_size * 2 + vocab_size) // 2, vocab_size)
        
    def forward(self, i, hs, init_hidden, att_w0):
        x = torch.unsqueeze(self.embedID(i), dim = 0)
        o, h = self.lstm(x, (init_hidden[0], torch.zeros_like(init_hidden[0])))
        a = torch.softmax(torch.mm(hs[0], torch.t(o[0])), dim = 0)
        a = torch.t(a)        

        #　初期Attention
        a = torch.cat((att_w0.unsqueeze(0), a[1:]), dim=0)
        
        c = torch.mm(a, hs[0])
        return self.l2(torch.tanh(self.l1(torch.cat((c, o[0]), dim=1))))

    def predict(self, i, hs, init_hidden, length, att_w02):
        h = (init_hidden[0], torch.zeros_like(init_hidden[0]))
        pred = []
        for j in range(length):
            x = torch.unsqueeze(self.embedID(i), dim = 0)
            o, h = self.lstm(x, h)
            if j == 0:
                a = att_w02.unsqueeze(0)
                #print(a.size(),"i = 0")
            else:
                a = torch.softmax(torch.mm(hs[0], torch.t(o[0])), dim = 0)
                a = torch.t(a)
                #print(a.size(),"i != 0") 
            #print(hs[0].size(), "hs[0]")
            c = torch.mm(a, hs[0])
            i = torch.unsqueeze(torch.argmax(self.l2(torch.tanh(self.l1(torch.cat((c, o[0]), dim=1))))), dim=0)
            pred.append(i.item())

        return pred
    
if __name__ == '__main__':
    i = torch.tensor([0, 1, 2, 3])
    o = torch.tensor([0, 1, 2, 3])
    encoder = Encoder(4, 256, 256)
    hs, h = encoder(i)
    decoder = Decoder(4, 256, 256)
    pred = decoder.predict(o[0:1], hs, h, len(o)-1) 
    print(pred)