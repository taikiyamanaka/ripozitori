import nltk, torch, model, random, pickle
import matplotlib.pyplot as plt


with open('text_vocab_size_test.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_size = pickle.load(f)
with open('summary_vocab_size_test.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_size = pickle.load(f)
with open('text_vocab_dict_swap_test.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict_swap = pickle.load(f)
with open('summary_vocab_dict_swap_test.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict_swap = pickle.load(f)

with open('corpus_test.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    corpus = pickle.load(f)  
    
 
print(text_vocab_size,"text size")
print(summary_vocab_size,"sum size")

encoder = model.Encoder(text_vocab_size, 256, 256)
decoder = model.Decoder(summary_vocab_size, 256, 256)
print("MODEL CREATED.")

encoder_optimizer = torch.optim.Adam(encoder.parameters())
decoder_optimizer = torch.optim.Adam(decoder.parameters())

criterion = torch.nn.CrossEntropyLoss()

loss_history = []

for epoch in range(100):
    loss = 0.0
    randomize_corpus = random.sample(corpus, len(corpus))
    count = 0
    for train in randomize_corpus:
        
        #print(corpus[n][1][2][1])
        #print(summary_vocab_dict_swap[corpus[n][1][2][1].item()]) #dmitri      
        count += 1
        print(count)
        subject_list = []
        #形態素解析
        for summary in train[1]:
            tmp_summary = []
            
            for word in summary:
                tmp_summary.append(summary_vocab_dict_swap[word.item()])
            #print(tmp_summary)
            #print(nltk.pos_tag(tmp_summary))
            
            #要約文から主語を獲得
            for tag in nltk.pos_tag(tmp_summary):
                #print(tag)
                if((tag[1] == "NNS" or tag[1] == "NN") and tag[0] != "<S>"):     
                    subject_list.append(tag[0])
                    break
            del tmp_summary
        #print(subject_list)

        #print("SUBJECT_LIST CREATED.")

        idx_list = [] 
            
        #記事から主語を検索
        for subject in subject_list:
            #print(subject)
            idx = 0
            #print(subject)
            #print(text_vocab_dict_swap[train[0].item()])
            for word in train[0]:
                    #print(idx)
                    #print(len(train[0]))
                    #print(text_vocab_dict_swap[word.item()])
                if text_vocab_dict_swap[word.item()] == subject:
                    idx_list.append(idx)                   
                    break
                if idx == len(train[0]) - 1:
                    idx_list.append(-1)
                    break
                idx += 1
        #記事の何番目に単語があるか
        #print(idx_list)
        
        #for text in train[0]:   
            #print(text_vocab_dict_swap[text.item()])
        
        #print("IDX_LIST CREATED.")

        flag = 0
        for i in range(len(idx_list)):
        
            att_w0 = []
        
            # 主語の単語が記事にない場合はスキップ
            if idx_list[i] == -1:
                continue
            #初めだけencoder処理
            if flag == 0:
                hs, h = encoder(train[0])
                
                for j in range(len(train[0])):
                    #print(len(train[0]),"train[0]")
                    if j == idx_list[i]:
                        #print(idx_list[i])
                        att_w0.append(1)
                    else:
                        att_w0.append(0)
                #print(att_w0)
                #print(torch.Tensor(att_w0))
                pred = decoder(train[1][i][:-1], hs, h, torch.Tensor(att_w0))
                del att_w0
                loss += criterion(pred, train[1][i][1:])
                flag = 1
            else:
                #print(train[1][0])
                #pred = decoder(train[1][:-1], hs, h)
                
                for j in range(len(train[0])):
                    #print(len(train[0]),"train[0]")
                    if j == idx_list[i]:
                        #print(idx_list[i])
                        att_w0.append(1)
                    else:
                        att_w0.append(0)
                #print(att_w0)
                
                pred = decoder(train[1][i][:-1], hs, h, torch.Tensor(att_w0))
                del att_w0
                #loss += criterion(pred, train[1][1:])
                loss += criterion(pred, train[1][i][1:])
                print(loss)
        #print("loss CALCULATED.")
    #loss /= len(corpus)
    
    loss_history.append(loss.item())
    
    print('Epoch', epoch+1, loss.item())
    encoder.zero_grad()
    decoder.zero_grad()
    loss.backward()
    encoder_optimizer.step()
    decoder_optimizer.step()
    
    #print(corpus[0][1][0][0:1]) # <S>を入れている 
    
    # n個目の記事を検証
    n = 0
    
    for i in range(len(corpus[n][1])):
        print('Original: {}'.format(' '.join([summary_vocab_dict_swap[x.item()] for x in corpus[n][1][i][1:-1]])))
    hs, h = encoder(corpus[n][0])
    #print(hs.size(),"hs")
    #print(corpus[n][0].size(),"corpus[n][0]")
    # 要約文の中で一番単語数が多い要約文の長さを抽出
    i = -1
    max = 0
    for sum in corpus[n][1]:
        i += 1
        if len(sum) > max:
            idx = i
    
    subject_list_tmp = []
    #形態素解析
    for summary in corpus[n][1]:
        tmp_summary = []
        
        for word in summary:
            tmp_summary.append(summary_vocab_dict_swap[word.item()])
            #print(tmp_summary)
            #print(nltk.pos_tag(tmp_summary))
            
        #要約文から主語を獲得
        for tag in nltk.pos_tag(tmp_summary):
                #print(tag)
            if((tag[1] == "NNS" or tag[1] == "NN") and tag[0] != "<S>"):     
                subject_list_tmp.append(tag[0])
                break
    print(subject_list_tmp)
    
    # 出力させたい単語を指定
    subject_0 = ["higgs"]
    
    idx_list2 = []
    #記事から主語を検索
    #for subject in subject_list_tmp:
    for subject in subject_0:
        #print(subject)
        idx2 = 0
        #print(subject)
        #print(text_vocab_dict_swap[train[0].item()])
        for word in corpus[n][0]:
                #print(idx)
                #print(len(train[0]))
                #print(text_vocab_dict_swap[word.item()])
            if text_vocab_dict_swap[word.item()] == subject:
                idx_list2.append(idx2)
                print(subject, "が見つかりました。この単語にAttentionをかけて生成します。")                 
                break
            if idx2 == len(corpus[n][0]) - 1:
                idx_list2.append(-1)
                print(subject, "が見つかりませんでした。")
                break
            idx2 += 1
    print(idx_list2)
    
    att_w02 = []
    
    # 何番目の要約文を出現させたいか
    sum_num = 0

    #print(subject_list_tmp[sum_num],"を主語として生成します。")       
    for j in range(len(corpus[n][0])):
        #print(len(train[0]),"train[0]")
        if j == idx_list2[sum_num]:
            #print(idx_list[i])
            att_w02.append(1)
        else:
            att_w02.append(0)
    
    #text内容を表示
    #print('{}'.format(' '.join([text_vocab_dict_swap[x.item()] for x in corpus[n][0]])))
    #print(torch.Tensor(att_w02).size(),"att_w02")
    pred = decoder.predict(corpus[n][1][0][0:1], hs, h, len(corpus[n][1][idx]) - 1, torch.Tensor(att_w02))
    print('Predict: {}'.format(' '.join([summary_vocab_dict_swap[x] for x in pred])))
    
    if loss.item() < 0.1: break

with open('encoder_test.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(encoder, f)                   # オブジェクトをシリアライズ  
with open('decoder_test.pickle', mode='wb') as g:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(decoder, g)                   # オブジェクトをシリアライズ      
 
plt.plot(loss_history)
plt.show()
