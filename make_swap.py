import nltk, torch, model, random, pickle

with open('text_vocab_dict_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict = pickle.load(f)
with open('summary_vocab_dict_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict = pickle.load(f)

text_vocab_dict_swap = {v: k for k, v in text_vocab_dict.items()}
summary_vocab_dict_swap = {v: k for k, v in summary_vocab_dict.items()}

with open('text_vocab_dict_swap_v2.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(text_vocab_dict_swap, f)                   # オブジェクトをシリアライズ   
with open('summary_vocab_dict_swap_v2.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(summary_vocab_dict_swap, f)                   # オブジェクトをシリアライズ   