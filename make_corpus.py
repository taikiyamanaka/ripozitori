import nltk, torch, model, random, pickle
    
# .keys() .values() .items()がタプル
#for k in summary_vocab_dict.keys():
    #if ':' in k:
        #print(k)
    #if "\'" in k:
        #print(k)
        
corpus = []
summaries_all = []
texts = []
count = 0

for line in open('corpus2.txt', encoding='utf-8'):

    count += 1
    elements = line.strip().split('\t')
    print(count)
    if count == 90001:
        break
    texts.append([x.lower() for x in nltk.word_tokenize(elements[0])])

    summaries_set = []

    for i in range(len(elements)-1):
        if elements[i+1] == "NULL":
            break
        else:
            summaries_set.append(["<S>"] + [x.lower() for x in nltk.word_tokenize(elements[i+1])] + ["</S>"])

    del  elements       
    summaries_all.append(summaries_set)

    del summaries_set

#with open('texts_v2.pickle', mode='wb') as f:              # with構文でファイルパスとバイナリ書き込みモードを設定
#    pickle.dump(texts, f)                               # オブジェクトをシリアライズ   
#with open('summaries_all_v2.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
#    pickle.dump(summaries_all, f)                   # オブジェクトをシリアライズ   

with open('text_vocab_dict_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    text_vocab_dict = pickle.load(f)
with open('summary_vocab_dict_v2.pickle', mode='rb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    summary_vocab_dict = pickle.load(f)

for t, summaries in zip(texts, summaries_all):
    tmp_summaries = []
    for s in summaries:
        #辞書に存在しない場合は1の<UNK>を入れている
        tmp_summaries.append(torch.tensor([summary_vocab_dict.get(x, 1) for x in s]))
    corpus.append((torch.tensor([text_vocab_dict.get(x, 1) for x in t]), tmp_summaries))
    del tmp_summaries
  
with open('corpus_v2.pickle', mode='wb') as f:  # with構文でファイルパスとバイナリ書き込みモードを設定
    pickle.dump(corpus, f)                   # オブジェクトをシリアライズ     